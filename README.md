# Konfigurasi AIR SDK
- Silahkan setting sendiri PATH SDK tempat anda menyimpan AIR SDK
- Gunakan AIR SDK minimal versi 15


# Cara Penggunaan Aplikasi
- Silahkan jalankan aplikasi seperti biasa
- Klik pada Area yang ingin diberi/dihilangkan pembatas


# Merubah pergerakan AI
Untuk merubah pergerakan AI silahkan ubah kode berikut:

private var isDiagonalCheck:Boolean = true;

True  --> AI bisa bergerak secara diagonal (8 arah)
False --> AI tidak bisa bergerak secara diagonal (4 arah)

# Merubah kecepatan AI
Untuk merubah kecepatan AI silahkan ubah kode berikut:

private var timer:Timer = new Timer(100); // satuan mili detik

Ganti nilai 100 menjadi berapapun, semakin kecil semakin cepat pergerakan AI.